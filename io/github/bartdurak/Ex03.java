package io.github.bartdurak;

import java.util.Scanner;
public class Ex03 {

        /**

         3. Poproś użytkownika o wpisanie tekstu, następnie wypisz go w kolejnej linii:

         Wpisz tekst: Witaj!
         Wpisany tekst to: Witaj!

         Do zaczytania tekstu użyj metody next klasy Scanner.
         Do konstruktora klasy Scanner przekaż domyślny strumień wejściowy: System.in

         */

        public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);

            System.out.print("Temat lekcji z histori: ");
            String text = scanner.next();
// uwaga nie przepisuj jak niże. Tylko podja faktyczny temat lekcji.
            System.out.print("Pierwsze zdanie pod ostatnim tematem: " + text);
        }
    }

